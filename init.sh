sudo apt update
sudo apt upgrade

echo -e "\033[1mInstalling git 🔀 ...\033[0m"
sudo apt install git

echo -e "\033[1mInstalling Vim 🗒️ ...\033[0m"
sudo apt install vim

echo -e "\033[1mInstalling zsh 💻 ...\033[0m"
sudo apt install zsh

echo -e "\033[1mInstalling Oh My Zsh 🦄...\033[0m"
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

wget -O $ZSH_CUSTOM/themes/pi.zsh-theme https://raw.githubusercontent.com/tobyjamesthomas/pi/master/pi.zsh-theme

echo -e "\033[1mInstalling tmux ➗ ...\033[0m"
sudo apt install tmux

echo -e "\033[1mInstalling Certbot 🤖 ...\033[0m"
sudo apt install certbot
sudo apt install python-certbot-nginx

echo -e "\033[1mInstalling node.js\033[0m"
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash

echo -e "\033[1mInstalling Yarn 🧶 ...\033[0m"
sudo apt install gnupg gnupg2 gnupg1
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -

echo -e "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update && sudo apt install yarn

echo -e "\033[1mCopying utils files 🛠️ ...\033[0m"

sudo ln -s `pwd`/gacp /usr/bin/gacp
sudo ln -s `pwd`/genpass /usr/bin/genpass
sudo ln -s `pwd`/goserver /usr/bin/goserver
sudo ln -s `pwd`/screenup /usr/bin/screenup

