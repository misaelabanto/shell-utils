# GACP
### Descripción
Realiza las siguientes tareas en una:
- Git status
- Git add .
- Git commit -m "mensaje"
- Git push origin \<rama\>

### Requisitos
- Git

### Uso
gacp \<branch-to-push\> \<commit-message\>

# GenPass
### Descripción
Crea una contraseña de una cantidad definida de caracteres
### Requisitos
- Node

### Uso
genpass \<nro-caracteres\>

